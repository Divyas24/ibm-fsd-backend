package springAnnotaionEx;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
@Configuration

public class EmployeeConfiguration {
	  @Bean
	    public Employee employee() {
	        return new Employee(address());
	    }    
	    
	    @Bean
	    public Address address() {
	        return new Address("Bangalore","India",583111);
	    }  
	
}
