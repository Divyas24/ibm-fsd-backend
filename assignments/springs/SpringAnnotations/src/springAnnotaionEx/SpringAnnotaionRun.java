package springAnnotaionEx;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

public class SpringAnnotaionRun {

		public static void main(String[] args) {
			// Assemble the objects
			AbstractApplicationContext context = new AnnotationConfigApplicationContext(EmployeeConfiguration.class);

			Employee obj = (Employee) context.getBean("employee");

			System.out.println(
					obj.getAddress().getCity() + " " + obj.getAddress().getCountry() + " " + obj.getAddress().getPin());
			
			context.registerShutdownHook();
		}
	}

	

