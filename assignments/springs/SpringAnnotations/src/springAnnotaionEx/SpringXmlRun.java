package springAnnotaionEx;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringXmlRun {
	public static void main(String[] args)
	{
	AbstractApplicationContext context = new ClassPathXmlApplicationContext("beans-annotation-config.xml");
	Employee obj = (Employee) context.getBean("employee");

	System.out.println(
			obj.getAddress().getCity() + " " + obj.getAddress().getCountry() + " " + obj.getAddress().getPin());
	
	context.registerShutdownHook();
}
}

