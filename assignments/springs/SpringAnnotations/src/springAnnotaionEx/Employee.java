package springAnnotaionEx;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Employee {
	public Address addr;
	public Address getAddr() {
	return addr;
}
public void setAddr(Address addr) {
	this.addr = addr;
}


	int id;
	String name;	
	LocalDate dob;	
	double salary;	
	Address address;
	String designnation;
	int age;
	boolean fulltime;
	List<String> skills;
	
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getDesignnation() {
		return designnation;
	}
	public void setDesignnation(String designnation) {
		this.designnation = designnation;
	}
	public boolean isFulltime() {
		return fulltime;
	}
	public void setFulltime(boolean fulltime) {
		this.fulltime = fulltime;
	}	
	public List<String> getSkills() {
		return skills;
	}
	public void setSkills(List<String> skills) {
		this.skills = skills;
	}
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	public Address getAddress() {
		return address;
	}
	
	
	public LocalDate getDob() {
		return dob;
	}
	public void setDob(LocalDate dob) {
		this.dob = dob;
	}
	@Autowired
	public Employee(Address address) {
		
		this.address = address;
	}

	
	/*public Employee(int id,String name,Address address)
	{
		this.id=id;
		this.name=name;
		this.address=address;
	}*/
	
}
