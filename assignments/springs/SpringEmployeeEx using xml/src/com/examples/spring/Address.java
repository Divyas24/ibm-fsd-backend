package com.examples.spring;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;

import org.springframework.context.annotation.ComponentScan;

import org.springframework.stereotype.Component;

public class Address {
	String city;
	String country;
	int pin;
	public String getCity() {
		return city;
	}
	
	public Address(String city, String country, int pin) {
		super();
		this.city = city;
		this.country = country;
		this.pin = pin;
	}
	@Override
	public String toString() {
		return "Address[city=" + city + ",country="+ country+ ",pin="+pin+"]";
	}

	public Address() {
		
	}

	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getPin() {
		return pin;
	}
	public void setPin(int pin) {
		this.pin = pin;
	}
	

}
