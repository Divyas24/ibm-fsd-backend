package com.examples.spring;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.stereotype.Component;
public class Employee {
	 int id;
		String name;
		LocalDate dob;
		
		Address address;
		 String destination;
		 public Address getAddress() {
			return address;
		}
		public void setAddress(Address address) {
			this.address = address;
		}
		double salary;
		 List<String> skills;
		
		
		boolean fulltime;
		public double getSalary() {
			return salary;
		}
		public List<String> getSkills() {
			return skills;
		}
		public void setSkills(List<String> skills) {
			this.skills = skills;
		}
		public void setSalary(double salary) {
			this.salary = salary;
		}
		public Employee(int id, String name, Address address) {
			this.id = id;
			this.name = name;
			this.address = address;
		}
		public int getId()
		{
			return id;
		}
		public String getName()
		{
			return name;
		}
		
		
		public LocalDate getDob() {
			return dob;
		}
		public void setDob(LocalDate dob) {
			this.dob = dob;
		}
		public String getDestination() {
			return destination;
		}
		public void setDestination(String destination) {
			this.destination = destination;
		}
		@Override

		public String toString() {

			return "Employee [id=" + id + ", name=" + name + ",dob=" + dob + ", salary=" + salary + ", destination=" + destination

					+ ", skills=" + skills + ", address=" + address + ", fulltime=" + fulltime + "]";

		}
		
		
		public boolean isFulltime() {
			return fulltime;
		}
		public void setFulltime(boolean fulltime) {
			this.fulltime = fulltime;
		}
		public void print()
		{
			System.out.println();
		}
		

}
