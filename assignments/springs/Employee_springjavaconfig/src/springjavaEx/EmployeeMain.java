package springjavaEx;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class EmployeeMain {

	public static void main(String[] args) {

		// Creating IoC container supplying configuration file from classpath
		AbstractApplicationContext context = new AnnotationConfigApplicationContext(EmployeeConfiguration.class);

		Employee obj = (Employee) context.getBean("employee");
		System.out.println(obj.getAddress().getCity() + " " + obj.getAddress().getCountry() + " " + obj.getAddress().getPin()+" "+obj.getDob()
		+" "+obj.getSkills());

	}
}
