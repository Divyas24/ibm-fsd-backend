
public class Employee {

		private int empId;
        private String name;
		private String designation;
        private String gender;
		private String department;
        private int age;
		private int salary;

		public Employee() {

			// this(100, "Anand", 35, "Male", "Sr.Developer", "IT", 50000);

			System.out.println("Employee class default constructor");

		}

		// overloaded custom constructor

		public Employee(int empId, String name, int age, String gender, String designation, String department, int salary) {

			this.name=name;
			this.gender=gender;
			this.age=age;
			this.empId = empId;
			this.designation = designation;
			this.department = department;
			this.salary = salary;

		}



		public int getEmpId() {

			return empId;

		}


		public void setEmpId(int empId) {

			this.empId = empId;

		}
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name=name;
		}
		
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender=gender;
		}
		public int getAge() {

			return age;

		}

		public void setAge(int age) {

			this.age = age;

		}


		public String getDesignation() {

			return designation;

		}



		public void setDesignation(String designation) {

			this.designation = designation;

		}



		public String getDepartment() {

			return department;

		}



		public void setDepartment(String department) {

			this.department = department;

		}



		public int getSalary() {

			return salary;

		}



		public void setSalary(int salary) {

			this.salary = salary;

		}


		public void printDetails() {

			// print employee details

			System.out.println("Print employee details");

			System.out.println("EmpId: " + this.getEmpId());

			System.out.println("Name: " + this.getName());

			System.out.println("Age: " + this.getAge());

			System.out.println("Gender: " + this.getGender());

			System.out.println("Designation: " + this.getDesignation());

			System.out.println("Department: " + this.getDepartment());

			System.out.println("Salary: " + this.getSalary());

			System.out.println();

		}


	}

