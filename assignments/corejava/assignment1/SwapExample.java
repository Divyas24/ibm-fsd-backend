
import java.*; 
  
class SwapExample{ 
  
    public static void main(String args[]) 
    { 
       Scanner scn=new Scanner(System.in);
       System.out.println("Enter X value");
       int x=scn.nextInt();
       System.out.println("Enter Y value");
       int y=scn.nextInt();
        System.out.println("Before swaping:"
                           + " x = " + x + ", y = " + y); 
        x = x + y; 
        y = x - y; 
        x = x - y; 
        System.out.println("After swaping:"
                           + " x = " + x + ", y = " + y); 
    } 
} 
  