import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class Employee implements EmployeeService{
 private static final AtomicInteger count = new AtomicInteger(-1); 
	//private static int count = 0;
 private int id;
 private String name;
 private int salary;
 private int age;
 private String dept;
 /*public Employee() {
	   
	    setEmpId(++count);
	}*/
 public interface ValidateEmployee {

		public boolean check(int age, double salary);

	}



	boolean validate(ValidateEmployee validator, int age, double salary) {

		return validator.check(age, salary);

	}

	public Employee( String name,int salary,String dept,  int age) {
		this.id=count.incrementAndGet(); 
		//this.id=id;
		this.name=name;
		this.salary=salary;
		this.age=age;
		this.dept=dept;
		
	}
	public int getId()
	{
		return id;
	}
	public void setEmpId(int id) {
		this.id = id;
	}
	public String getName()
	{
		return name;
	}
	public void setEmpName(String name) {
		this.name = name;
	}
	public int getAge()
	{
		return age;
	}
	public void setAge(int age)
	{
		this.age=age;
	}
	public int getSalary()
	{
		return salary;
	}
	public void setSalary(int salary)
	{
		this.salary=salary;
	}
	public String getDept()
	{
		return dept;
	}
	public void setDept(String dept)
	{
		this.dept=dept;
	}
	
	@SuppressWarnings("rawtypes")
	Comparator<Employee> EMPLOYEE_SORT_BY_NAME = new Comparator<Employee>() {

		@Override

		public int compare(Employee o1, Employee o2) {

			 if (o1 instanceof Employee && o2 instanceof Employee) {

			 return ((Employee) o1).getName().compareTo(((Employee) o2).getName());

			 }

		return 0;
		}

	};
	TreeMap<Integer,Employee> employee=new TreeMap<>();
	Scanner sc=new Scanner(System.in);
	@Override
	public void add()
	{
		
		System.out.println("enter employee name");

		String name = sc.nextLine();

		boolean flag = false;

		String rename = "[a-zA-Z ]+";

		while (!flag) {

			if ((!name.isEmpty() && name.matches(rename))) {



				flag = true;



			} else {

				System.out.println("enter only alphabets or please enter ur name");

				name = sc.nextLine();

			}

		}

		System.out.println("enter employee salary");

		int salary =Integer.parseInt(sc.nextLine());


				System.out.println("enter employee age");

				age = Integer.parseInt(sc.nextLine());

	

		System.out.println("enter employee designation");

		String dept = sc.nextLine();

		boolean flag2 = true;

		while (flag2) {

			if (!dept.isEmpty() && dept.matches(rename)) {

				flag2 = false;

			} else {

				System.out.println("enter only alphabets");
				 dept = sc.nextLine();

			}

		}

		
		

		employee.put(getId(), new Employee(name, salary, dept, age));

		System.out.println("employee added successfully");
	}
	
	public void print()
	{
		List<Employee> emplSo = new ArrayList<Employee>(employee.values());
		Collections.sort(emplSo, EMPLOYEE_SORT_BY_NAME);

		if (!emplSo.isEmpty()) {
			System.out.format("EmpId \t EmpName \t Age \t Department \t Salary\n");
			for (Employee empp : emplSo) {

				System.out.format(" %d \t %s \t %d \t %s \t %d\n", empp.id, empp.name, 
						empp.age, empp.dept, empp.salary);

			}
		} else {
			System.out.println("No details available..please isert Employee details");
		}}


	@Override
	public String toString() {
		return "EmployeeDetails [empId=" + id + ", empName=" + name + ", age=" + age
				+ ",  department=" + dept + ", salary=" + salary  + "]";
	}
	
	/*public Employee() {
		
	}*/
	@Override
	public void delete() {
	
		System.out.println("enter the key which u want to delete");

		int did = Integer.parseInt(sc.nextLine());

		boolean bool = true;

		while (bool) {

			if (employee.containsKey(did)) {

				employee.remove(did);

				bool = false;

			} else {

				System.out.println("ur entered employee doesnt exist");

			}

		}
		
	}
  @Override
	public void update() {
	  
	  System.out.println("enter employee name");

		String name = sc.nextLine();

		boolean flag = false;

		String rename = "[a-zA-Z ]+";

		while (!flag) {

			if ((name.matches(rename))) {



				flag = true;



			} else {

				System.out.println("enter only alphabets");

				name = sc.nextLine();

			}

		}

		System.out.println("enter employee salary");

		int salary =Integer.parseInt(sc.nextLine());



		System.out.println("enter employee age");

		int age = Integer.parseInt(sc.nextLine());

		boolean flag1 = true;

		while (flag1) {

			boolean ageValue = validate((age1, salary1) -> age1 > 20 && salary1 > 30000



					, age, salary);



			if (ageValue) {

				System.out.println("valid age and salary");

				flag1 = false;

			} else {

				System.out.println("invalid age or salary");

				System.out.println("enter employee age");

				age = Integer.parseInt(sc.nextLine());

				System.out.println("enter employee salary");

				salary =Integer.parseInt(sc.nextLine());

			}

		}

		System.out.println("enter employee designation");

		String des = sc.nextLine();

		employee.get(id).setEmpName(name);



		employee.get(id).setSalary(salary);

		employee.get(id).setAge(age);

		employee.get(id).setDept(dept);

		System.out.println("employee updated successfully");


		
	}


	@Override
	public void stat()
	{
		Map<String, Long> count1;

		Map<String, Double> avgAge;

		long count2 = 0;

		long countValue = employee.size();

		double avgSal = 0;

		Map<String, Long> list3Dep;

		List emp = new ArrayList();

		List<Employee> list = new ArrayList<Employee>(employee.values());

		count2 = list.stream().filter(e -> e.getAge() < 30).count();

		System.out.println("no of employees age older than 30 is" + count2);

		emp = list.stream().filter(e -> e.getAge() < 30).map(e -> e.getId()).collect(Collectors.toList());

		System.out.println("list employee Ids older than 30 years" + emp);

		count1 = list.stream().collect(Collectors.groupingBy(Employee::getDept, Collectors.counting()));

		System.out.println("no of employees in each departament" + count1);

		double totSalary = list.stream().map(Employee::getSalary)

				.reduce(0, (a, b) -> a.intValue() + b.intValue()).intValue();

		System.out.println("total salary is:" + totSalary);

		double avgSalary = list.stream().collect(Collectors.averagingInt(Employee::getSalary));

		System.out.println("average of salary is" + avgSalary);

		avgAge = list.stream()

				.collect(Collectors.groupingBy(Employee::getDept, Collectors.averagingInt(Employee::getAge)));

		System.out.println("avg age of employees by department" + avgAge);



		List<String> totalMore2 = list.stream().collect(Collectors.groupingBy(Employee::getDept, Collectors.counting()))

				.entrySet().stream().filter(e -> e.getValue() > 3).map(Map.Entry::getKey).collect(Collectors.toList());

		System.out.println(totalMore2);

		List<String> slist = list.stream().filter(e -> e.getName().startsWith("s")).map(Employee::getName)

				.collect(Collectors.toList());

		System.out.println(slist);

		Map<String, Long> ordlist = count1.entrySet().stream().sorted(Comparator.comparing(Map.Entry::getValue))

				.collect(Collectors.groupingBy(Map.Entry::getKey, TreeMap::new, Collectors.counting()));

		System.out.println(ordlist);
				
			}
		
		  }
	
	

