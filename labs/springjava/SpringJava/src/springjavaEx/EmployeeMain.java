package springjavaEx;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class EmployeeMain {

	public static void main(String[] args) {

		// Creating IoC container supplying configuration file from classpath
		AbstractApplicationContext context = new AnnotationConfigApplicationContext(EmployeeConfiguration.class);

		/*// Retrieving the bean from container
		 Address obj = (Address) context.getBean("address");
		Employee obj1 = (Employee) context.getBean("employee");

		// Display message
		System.out.println("Emp id:" +obj1.getId());
		System.out.println("Emp name:" +obj1.getName());
		System.out.println("Emp age:" +obj1.getAge());
		System.out.println("Emp dob:" +obj1.getDob());
		System.out.println("Emp dsgn:" +obj1.getDesignnation());
		System.out.println("Emp salary:" +obj1.getSalary());
		System.out.println("Emp skills:" +obj1.getSkills());
		System.out.println("Emp adress:" +obj1.getAddress());
		System.out.println("Emp fulltime:" +obj1.isFulltime());
		// Graceful shutdown of container
		context.registerShutdownHook();*/
		
		Employee obj = (Employee) context.getBean("employee");
		System.out.println(obj.getAddress().getCity() + " " + obj.getAddress().getCountry() + " " + obj.getAddress().getPin()+" "+obj.getDob()
		+" "+obj.getSkills());

	}
}
