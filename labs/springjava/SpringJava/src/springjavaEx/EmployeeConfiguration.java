package springjavaEx;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
@Configuration
public class EmployeeConfiguration {

	 @Bean
	    public Employee employee() {
	        return new Employee(address(),localdate(),skills());
	    }    
	    
	    @Bean
	    public Address address() {
	        return new Address("Bangalore","India",583111);
	    } 
	    @Bean
	    public LocalDate localdate() {
	    	return LocalDate.parse("2019-03-22");
	    }
	    @Bean
	    public List<String> skills() {
	    	ArrayList<String> abc=new ArrayList<>();
	    	abc.add("developer");
	    	abc.add("tester");
	    	return abc;
	    }
	    
	    
}
