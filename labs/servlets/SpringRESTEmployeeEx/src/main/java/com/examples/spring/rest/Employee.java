package com.examples.spring.rest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Employee {
 private int id;
 private String name;
 private String dept;
 private int age;
 private int salary;
 public int getId() {
	return id;
}
public void setId(int string) {
	this.id = string;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getDept() {
	return dept;
}
public void setDept(String dept) {
	this.dept = dept;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
public int getSalary() {
	return salary;
}
public void setSalary(int salary) {
	this.salary = salary;
}

}
