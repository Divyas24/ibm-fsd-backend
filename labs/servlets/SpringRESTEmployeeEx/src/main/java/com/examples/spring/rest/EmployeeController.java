package com.examples.spring.rest;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class EmployeeController {
	@RequestMapping(value = { "/", "/employee" }, method = RequestMethod.GET)
	public String employee() {
		return "Welcome to RESTful services training :)";
	}
	
	@RequestMapping(value = "/employee", method = RequestMethod.GET, params = { "msg" })
	public String getEmployee(@RequestParam(required = false, defaultValue = "Hello") String msg) {
		return "Welcome to RESTful services training :) - " + msg;
	}
	
	@RequestMapping(value = "/employee", method = RequestMethod.POST, consumes = { "application/xml",
	"application/json" }, produces = { "application/xml", "application/json" })
public Employee postEmployeeObject(@RequestBody Employee employee) {
employee.setName(employee.getName() + " - POST object mapping example");
return employee;
}
	@RequestMapping(value = "/employee", method = RequestMethod.PUT)
	public Map<Integer, Employee> putEmployeeWithPathVariable(@RequestBody Employee employee
			) {
		Map<Integer,Employee> list=new HashMap<Integer,Employee>();
		list.put(1,employee);
		return  list;
	}
}
