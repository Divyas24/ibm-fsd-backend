package servletEx;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.Enumeration;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EmployeeRegister extends HttpServlet {
	public void init()
	{
		System.out.println("hi");
	}
	public void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException
	{
		System.out.println("get method");
        this.doPost(request, response);
	}
	
	public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException
	{
		System.out.println("context path:" + request.getContextPath());
        String firstname=request.getParameter("fname");
        String lastname=request.getParameter("lname");
		response.setContentType("text/html");
		System.out.println("invoked");
		
			 PrintWriter out = response.getWriter();
			 out.println("<!DOCTYPE html>");
			 out.println("<html><body>");
			out.println("firstname :"+ firstname);
			out.println("lastname :"+ lastname);
			out.println("<a href='index.html'>Home</a>");
			out.println("</body></html>");
	}
	public void destroy()
	{
		System.out.println("destroyed");
	}
}
