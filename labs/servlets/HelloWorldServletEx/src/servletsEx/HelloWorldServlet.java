package servletsEx;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloWorldServlet extends HttpServlet{
	public void init()
	{
		System.out.println("helloworld servlet initialized...");
	}
	public void service(HttpServletRequest request,HttpServletResponse response) throws IOException
	{
		System.out.println("helloworld servlet invoked...");
        PrintWriter out=response.getWriter();
		out.println("hello world");
		out.println("current date:" + LocalDate.now());
	}
	public void destroy()
	{
		System.out.println("helloworld servlet destroyed...");

	}
}