package com.examples.spring.web.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloWorldController {
	@RequestMapping(value="/",method=RequestMethod.GET,produces="text/plain")
	@ResponseBody
public String sayHello() {
		System.out.println("Spring Rest Hello");
	return "HelloWorld from Spring REST";
}
	
	@GetMapping(value="/greeting")
	@ResponseBody
public String greeting1(String msg) {
		System.out.println("Spring Rest Hello");
	return "HelloWorld from Spring REST without param";
}
	
	@GetMapping(value="/greeting",params= {"msg"})
	@ResponseBody
public String greeting2(String msg) {
		System.out.println("Spring Rest Hello");
	return "HelloWorld from Spring REST with param"+ msg;
}
	@PostMapping(value="/greeting",consumes={"text/plain","text/html"})
	@ResponseBody
public String postgreeting(@RequestBody String reqBody) {
		System.out.println("Spring Rest Hello");
	return "the msg received:"+ reqBody;
}
	@PutMapping(value="/greeting")
	@ResponseBody
public String putgreeting(String msg) {
		System.out.println("Spring Rest Hello");
	return "HelloWorld from Spring REST PUT";
}
	@DeleteMapping(value="/greeting")
	@ResponseBody
public String deletegreeting(String msg) {
		System.out.println("Spring Rest Hello");
	return "HelloWorld from Spring REST DELETE";
}
}
