package servletEx;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.Enumeration;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloWorldServlet extends HttpServlet{
	public void init()
	{
		System.out.println("hi");
	}
	public void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException
	{
		System.out.println("get method");
        this.doPost(request, response);
	}
	
	public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException
	{
		System.out.println("context path:" + request.getContextPath());
        Enumeration<String> headerNames=request.getHeaderNames();
		while(headerNames.hasMoreElements())
		{
		String headerName=headerNames.nextElement();
		System.out.println(headerName+ ":"+ request.getHeader(headerName));
		}
		response.setContentType("text/html");
		response.setIntHeader("refresh", 5);
		System.out.println("invoked");
		
			 PrintWriter out = response.getWriter();
			 out.println("<!DOCTYPE html>");
			 out.println("<html><body>");
			out.println("hello world");
			out.println("curret date:"+LocalDateTime.now());
			out.println("</body></html>");
	}
	public void destroy()
	{
		System.out.println("destroyed");
	}
}