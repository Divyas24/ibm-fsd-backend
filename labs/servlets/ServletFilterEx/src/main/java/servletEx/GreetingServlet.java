package servletEx;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet(name = "Login")
public class GreetingServlet extends HttpServlet{
	  
	public void init()
	{
		System.out.println("hi");
	}
	  
	public void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException
	{
		System.out.println("get method");
        this.doPost(request, response);
	}
	  
	public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException
	{
		
		
		response.setContentType("text/html");
		
		System.out.println("invoked");
		
			 PrintWriter out = response.getWriter();
			 out.println("<!DOCTYPE html>");
			 out.println("<html><body>");
			out.println("hello world");
			out.println("welcome to servlets demo");
			out.println("</body></html>");
	}
	  
	public void destroy()
	{
		System.out.println("destroyed");
	}
}
