package com.examples.java.jsp;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class EmpReg extends HttpServlet{
  public String name;
  public int password;
  public int age;
  public String gender;
  public String city;
  public String skills;
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getPassword() {
	return password;
}
public void setPassword(int password) {
	this.password = password;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public String getSkills() {
	return skills;
}
public void setSkills(String skills) {
	this.skills = skills;
}
public void init() {
	ServletContext context=getServletContext();
	Enumeration<String> paramNames=context.getInitParameterNames();
	while(paramNames.hasMoreElements()) {
		String paramName=paramNames.nextElement();
		System.out.println(paramName+":"+context.getInitParameter(paramName));
	}
}
protected void doGet(HttpServletRequest request,HttpServletResponse response)
throws ServletException,IOException{
	this.doPost(request, response);
}
protected void doPost(HttpServletRequest request,HttpServletResponse response)
throws ServletException,IOException{
	HttpSession session=request.getSession();
	System.out.println(session.getId());
	response.setContentType("text/html");
	name=request.getParameter("uname");
	password=request.getParameter("upassword");
    age=request.getParameter("uage");
    gender=request.getParameter("ugender");
    city=request.getParameter("ucity");
    skills=request.getParameter("uskills");
	session.setAttribute(name, "uname");
	session.setAttribute(age, "uage");
	session.setAttribute(password, "upassword");
	session.setAttribute(gender, "ugender");
	session.setAttribute(city, "ucity");
    session.setAttribute(skills, "uskills");
	RequestDispatcher rd=request.getRequestDispatcher("register.jsp");
	rd.forward(request, response);
}
}
