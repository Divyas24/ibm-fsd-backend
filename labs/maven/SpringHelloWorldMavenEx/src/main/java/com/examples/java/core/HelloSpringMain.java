package com.examples.java.core;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HelloSpringMain {

	
	public static void main(String[] args) {

		// Instantiating the container
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans-config.xml");

		// Access bean from container
		Greetings obj = (Greetings) context.getBean("greetings");
		obj.getMessage();
//		Greetings obj1 = (Greetings) context.getBean("greetings1");
//		obj1.getMessage();
//		Greetings obj2 = (Greetings) context.getBean("greetings2");
//		obj2.getMessage();
		// Graceful shutdown of container, preferred for stand alone apps
		context.registerShutdownHook();

		// Graceful shutdown of container, has same effect as thing as
		// registering shutdown hook, but not be called when JVM goes down
		// before reaching this statement
		// context.close();

	}

}
